#!/bin/bash
red="\033[31m"
black="\033[0m"

base=/etc/dnat
if [ ! -d $base ];then mkdir -p $base;fi
conf=$base/conf
if [ ! -f $conf [;then touch $conf;fi

# wget -qO natcfg.sh http://arloor.com/sh/iptablesUtils/natcfg.sh && bash natcfg.sh
echo -e "${red}用途${black}: 便捷的设置iptables端口转发"
echo -e "${red}新增${black}: 针对 TCP+UDP|TCP|UDP 单独设置"
echo -e "${red}新增${black}: 添加域名转发时支持设置多个端口"

type=UDP
echo "选择设置类型:"
select type in $(echo TCP UDP ALL);do break;done

echo
setupService(){
    echo '等待操作应用……'
    wget -qO /usr/local/bin/dnat.sh https://gitlab.com/yovfal/publicshell/raw/master/dnat.sh || {
        echo "脚本不存在，请通过github提交issue通知作者"
        exit 1
    }
    echo 


echo "[Unit]
Description=动态设置iptables转发规则
After=network-online.target
Wants=network-online.target

[Service]
WorkingDirectory=/root/
EnvironmentFile=
ExecStart=/bin/bash /usr/local/bin/dnat.sh $type
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target " > /lib/systemd/system/dnat.service

systemctl daemon-reload
systemctl enable dnat > /dev/null 2>&1
service dnat stop > /dev/null 2>&1
service dnat start > /dev/null 2>&1
echo '操作完成！'
}


## 获取本机地址
localIP=$(ip -o -4 addr list | grep -Ev '\s(docker|lo)' | awk '{print $4}' | cut -d/ -f1 | grep -Ev '(^127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$)|(^10\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$)|(^172\.1[6-9]{1}[0-9]{0,1}\.[0-9]{1,3}\.[0-9]{1,3}$)|(^172\.2[0-9]{1}[0-9]{0,1}\.[0-9]{1,3}\.[0-9]{1,3}$)|(^172\.3[0-1]{1}[0-9]{0,1}\.[0-9]{1,3}\.[0-9]{1,3}$)|(^192\.168\.[0-9]{1,3}\.[0-9]{1,3}$)')
if [ "${localIP}" = "" ]; then
        localIP=$(ip -o -4 addr list | grep -Ev '\s(docker|lo)' | awk '{print $4}' | cut -d/ -f1|head -n 1 )
fi

rmIptablesNat(){
    #删除旧的中转规则
        local arr1=(`iptables -L PREROUTING -n -t nat --line-number |grep DNAT|grep "dpt:$1 "|sort -r|awk '{print $1,$3,$9}'|tr " " ":"|tr "\n" " "`)
        for cell in ${arr1[@]}  # cell= 1:tcp:to:8.8.8.8:543
        do
            local arr2=(`echo $cell|tr ":" " "`)  #arr2=(1 tcp to 8.8.8.8 543)
            local index=${arr2[0]}
            local proto=${arr2[1]}
            local targetIP=${arr2[3]}
            local targetPort=${arr2[4]}
            # echo 清除本机$localport端口到$targetIP:$targetPort的${proto}的PREROUTING转发规则[$index]
            iptables -t nat  -D PREROUTING $index
            # echo ==清除对应的POSTROUTING规则
            local toRmIndexs=(`iptables -L POSTROUTING -n -t nat --line-number|grep SNAT|grep $targetIP|grep dpt:$targetPort|grep $proto|awk  '{print $1}'|sort -r|tr "\n" " "`)
            for cell1 in ${toRmIndexs[@]}
            do
                iptables -t nat  -D POSTROUTING $cell1
            done
        done
}

addDnat(){
    local localport=
    local remoteport=
    local remotehost=
    local valid=
    read -p "输入本地端口号(多个端口以空格分开，此时目标端口等于本地端口):" localport 
    if $(echo $localport | grep -q ' ');then
    
        read -p "输入目标域名:" remotehost 
        until ! $(echo  $remotehost |grep -E -o '([0-9]{1,3}[\.]){3}[0-9]{1,3}');do
            echo "${remotehost} 输入错误！规则：非ip"
            read -p "输入目标域名:" remotehost 
        done
        
        for val in $(echo $localport);do
            until $(echo ${val} | grep -q '^[0-9]*$');do
                echo "${val} 输入错误！规则：数字"
                read -p "输入本地端口号:" val 
            done
            
            remoteport=$val
            localport=$val
    
            sed -i "s/^$localport.*/$localport>$remotehost:$remoteport/g" $conf
            grep -q "$localport>$remotehost:$remoteport" $conf || echo "$localport>$remotehost:$remoteport" >>$conf
            echo "成功添加转发规则 $localport>$remotehost:$remoteport 大约两分钟后规则会生效"
        done
        setupService
    else
        read -p "输入远程端口号:" remoteport 
        until $(echo ${remoteport} | grep -q '^[0-9]*$');do
            echo "${remoteport} 输入错误！规则：数字"
            read -p "输入本地端口号:" remoteport 
        done
    
        read -p "输入目标域名:" remotehost 
        until ! $(echo  $remotehost |grep -E -o '([0-9]{1,3}[\.]){3}[0-9]{1,3}');do
            echo "${$remotehost} 输入错误！规则：非ip"
            read -p "输入目标域名:" remotehost 
        done
    
        sed -i "s/^$localport.*/$localport>$remotehost:$remoteport/g" $conf
        grep -q "$localport>$remotehost:$remoteport" $conf || echo "$localport>$remotehost:$remoteport" >>$conf
        setupService
        echo "成功添加转发规则 $localport>$remotehost:$remoteport 大约两分钟后规则会生效"
    fi
}

rmDnat(){
    local localport=
    PS3=输入需要删除转发序号:
    select localport in $(cat $conf);do break;done
    sed -i "${REPLY}d" $conf

    rmIptablesNat ${localport%>*}
    #删除临时文件  
    rm -f $base/${localport%>*}IP
    echo "删除 $localport 完成！"
}

testVars(){
    local localport=$1
    local remotehost=$2
    local remoteport=$3
    # 判断端口是否为数字
    local valid=
    echo "$localport"|[ -n "`sed -n '/^[0-9][0-9]*$/p'`" ] && echo $remoteport |[ -n "`sed -n '/^[0-9][0-9]*$/p'`" ]||{
       # echo  -e "${red}本地端口和目标端口请输入数字！！${black}";
       return 1;
    }

    # 检查输入的不是IP
    if [ "$(echo  $remotehost |grep -E -o '([0-9]{1,3}[\.]){3}[0-9]{1,3}')" != "" ];then
        local isip=true
        local remote=$remotehost

        # echo -e "${red}警告：你输入的目标地址是一个ip!${black}"
        return 2;
    fi
}

lsDnat(){
    arr1=(`cat $conf`)
for cell in ${arr1[@]}  
do
    arr2=(`echo $cell|tr ":" " "|tr ">" " "`)  #arr2=16 REJECT 0.0.0.0/0
    # 过滤非法的行
    [ "${arr2[2]}" != "" -a "${arr2[3]}" = "" ]&& testVars ${arr2[0]}  ${arr2[1]} ${arr2[2]}&&{
        echo "转发规则： ${arr2[0]}>${arr2[1]}:${arr2[2]}"
    }
done
}

addSnat(){
    local localport=
    local remoteport=
    local remotehost=
    echo -n "本地端口号:" ;read localport
    echo -n "远程端口号:" ;read remoteport
    # echo $localport $remoteport
    # 判断端口是否为数字
    echo "$localport"|[ -n "`sed -n '/^[0-9][0-9]*$/p'`" ] && echo $remoteport |[ -n "`sed -n '/^[0-9][0-9]*$/p'`" ]||{
        echo  -e "${red}本地端口和目标端口请输入数字！！${black}"
        return 1;
    }

    echo -n "目标IP:" ;read remotehost
    # 检查输入的不是IP
    if [ "$remotehost" = "" -o "$(echo  $remotehost |grep -E -o '([0-9]{1,3}[\.]){3}[0-9]{1,3}')" != "" ];then
        rmIptablesNat $localport

        ## 建立新的中转规则
        if [ "$type" == "TCP" ];then
            iptables -t nat -A PREROUTING -p tcp --dport $localport -j DNAT --to-destination $remote:$remoteport
            iptables -t nat -A POSTROUTING -p tcp -d $remote --dport $remoteport -j SNAT --to-source $localIP
        elif [ "$type" == "UDP" ];then
            iptables -t nat -A PREROUTING -p udp --dport $localport -j DNAT --to-destination $remote:$remoteport
            iptables -t nat -A POSTROUTING -p udp -d $remote --dport $remoteport -j SNAT --to-source $localIP
        else
            iptables -t nat -A PREROUTING -p tcp --dport $localport -j DNAT --to-destination $remote:$remoteport
            iptables -t nat -A PREROUTING -p udp --dport $localport -j DNAT --to-destination $remote:$remoteport
            iptables -t nat -A POSTROUTING -p tcp -d $remote --dport $remoteport -j SNAT --to-source $localIP
            iptables -t nat -A POSTROUTING -p udp -d $remote --dport $remoteport -j SNAT --to-source $localIP
        fi
    else
        echo 请输入一个IP
        return 1
    fi    
}

rmSnat(){
    local localport=
    echo -n "本地端口号:" ;read localport
    echo "$localport"|[ -n "`sed -n '/^[0-9][0-9]*$/p'`" ] &&rmIptablesNat $localport
}


main(){
echo  -e "\n————————————————————\n"
PS3="请选择："
select todo in 增加到域名的转发 删除到域名的转发 增加到IP的转发 删除到IP的转发 列出所有到域名的转发 查看iptables转发规则
do
    case $todo in
    增加到域名的转发)
        addDnat
        main
        ;;
    删除到域名的转发)
        rmDnat
        main
        ;;
    增加到IP的转发)
        addSnat
        main
        ;;
    删除到IP的转发)
        rmSnat
        main
        ;;
    列出所有到域名的转发)
        lsDnat
        main
        ;;
    查看iptables转发规则)
        iptables -L PREROUTING -n -t nat --line-number
        iptables -L POSTROUTING -n -t nat --line-number
        main
        ;;
    *)
        echo "如果要退出，请按Ctrl+C"
        ;;
    esac
done
}
main